package ca.theGreatTrail.tgt.data.manager

import android.content.Context
import android.content.SharedPreferences
import androidx.core.content.edit

object UserManager {

    private const val PREF_NAME = "vn.spaceshare.cms.user"
    private const val ACCESS_TOKEN_SS = "access_token_spaceshare"
    private const val FIRE_BASE_TOKEN = "fire_base_token"
    private const val USER_ID = "user_id"
    private const val FULL_NAME = "full_name"
    private const val PROFILE_IMAGE_THUMB = "profile_image_thumb"
    private const val PROFILE_IMAGE = "PROFILE_IMAGE"
    private const val EMAIL = "email"
    private const val PHONE = "phone"
    private const val NATIONAL_CODE = "national_code"
    private const val ADDRESS = "address"
    private const val IS_LOGGED_IN = "is_logged_in"
    private const val NOTIFY_COUNT = "notify_count"

    private fun getPrefs(context: Context): SharedPreferences =
        context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)

    /**
     * Authorization token obtained via Authenticate step
     * Used to access private resources on backend server
     * Token life time lasts 1 day, after that it will be rejected and not valid anymore
     */
    fun setAccessToken(context: Context, token: String) {
        getPrefs(context).edit { putString(ACCESS_TOKEN_SS, token) }
    }

    /**
     * Please read [setAccessToken] for more detail
     */
    fun getAccessToken(context: Context): String {
        val token = getPrefs(context).getString(ACCESS_TOKEN_SS, "") ?: ""

        return when {
            token.isEmpty() -> ""

            else -> "JWT $token"
        }
    }

    //Return token without JWT
    fun getToken(context: Context): String = getPrefs(context).getString(ACCESS_TOKEN_SS, "") ?: ""



    /**
     * Set and get User Id
     */
    fun setUserId(context: Context, userId: Int) {
        getPrefs(context).edit { putInt(USER_ID, userId) }
    }

    fun getUserId(context: Context): Int = getPrefs(context).getInt(USER_ID, 0)

    /**
     * Set and get Full Name
     */
    fun setFulLName(context: Context, fullName: String) {
        getPrefs(context).edit { putString(FULL_NAME, fullName) }
    }

    fun getFullName(context: Context): String = getPrefs(context).getString(FULL_NAME, "") ?: ""

    /**
     * Set and get Profile Image thumb
     */
    fun setProfileImageThumb(context: Context, profileImageThumb: String) {
        getPrefs(context).edit { putString(PROFILE_IMAGE_THUMB, profileImageThumb) }
    }

    fun getProfileImageThumb(context: Context): String = getPrefs(context).getString(PROFILE_IMAGE_THUMB, "") ?: ""

    /**
     * Set and get Profile Image
     */
    fun setProfileImage(context: Context, profileImage: String) {
        getPrefs(context).edit { putString(PROFILE_IMAGE, profileImage) }
    }

    fun getProfileImage(context: Context): String = getPrefs(context).getString(PROFILE_IMAGE, "") ?: ""

    /**
     * Set and get Email
     */
    fun setEmail(context: Context, email: String) {
        getPrefs(context).edit { putString(EMAIL, email) }
    }

    fun getEmail(context: Context): String = getPrefs(context).getString(EMAIL, "") ?: ""

    /**
     * Set and get phone number
     */
    fun setPhone(context: Context, phone: String) {
        getPrefs(context).edit { putString(PHONE, phone) }
    }

    fun getPhone(context: Context): String = getPrefs(context).getString(PHONE, "")?.trim() ?: ""

    /**
     * Set and get national code
     */
    fun setNationalCode(context: Context, nationalCode: String) {
        getPrefs(context).edit { putString(NATIONAL_CODE, nationalCode) }
    }

    fun getNationalCode(context: Context): String =
        getPrefs(context).getString(NATIONAL_CODE, "") ?: ""

    fun getPhoneWithNationCode(context: Context): String {
        return "${getNationalCode(context)} ${getPhone(context)}"
    }

    /**
     * Set and get Adress
     */
    fun setAddress(context: Context, address: String) {
        getPrefs(context).edit { putString(ADDRESS, address) }
    }

    fun getAddress(context: Context): String = getPrefs(context).getString(ADDRESS, "") ?: ""

    /**
     * Set and get Log in state
     */
    fun setLoggedIn(context: Context, isLoggedIn: Boolean) {
        getPrefs(context).edit { putBoolean(IS_LOGGED_IN, isLoggedIn) }
    }

    fun getLoginState(context: Context): Boolean = getPrefs(context).getBoolean(IS_LOGGED_IN, false)

    fun clear(context: Context) {
        getPrefs(context).edit { clear() }
    }
}