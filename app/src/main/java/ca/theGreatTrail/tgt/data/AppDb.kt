package ca.theGreatTrail.tgt.data

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters

@TypeConverters(

)
abstract class AppDb : RoomDatabase() {
}