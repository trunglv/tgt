package ca.theGreatTrail.tgt.domain.remote.pojo.response

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

open class BaseResponse<T> {
    @Expose
    @SerializedName("status")
    val status: Boolean = false

    @Expose
    @SerializedName("message_code")
    val messageCode: String = ""

    @Expose
    @SerializedName("message_params")
    val messageParams: Any? = null

    @Expose
    @SerializedName("data")
    val data: T? = null
}