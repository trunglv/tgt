package ca.theGreatTrail.tgt.domain.remote.api

import android.annotation.SuppressLint
import android.app.Application
import okhttp3.Authenticator
import okhttp3.Request
import okhttp3.Response
import okhttp3.Route
import timber.log.Timber

class Authenticator(private val app: Application) : Authenticator {

    lateinit var apiService: ApiServiceNoAuth

    @SuppressLint("CheckResult")
    override fun authenticate(route: Route?, response: Response?): Request? {
        Timber.d("authenticate() called [401 error]")

        /*response?.let {
            Timber.d("Failed response count: ${responseCount(response)}")
            if (responseCount(response) >= 2) {
                return null
            }
        }

        var obtained = false

        apiService.refreshToken(UserHostManager.getAccessToken(app))
            .subscribeWith(
                {},
                {},
                {}
            )
           *//* .subscribeWith(object : DisposableSingleObserver<Token>() {

                override fun onSuccess(t: Token) {
                    Timber.d("onNext() called with dat: %s", t.toString())
                    with(UserHostManager) {
                        setAccessToken(app, t.token)
                    }

                    obtained = true
                }

                override fun onError(e: Throwable) {
                    Timber.d("onError() called")
                    Timber.w(e)
                    obtained = false
                }
            }*//*)

        if (obtained) return response?.let {
            response.request()
                .newBuilder()
                .addHeader("Authorization", UserHostManager.getAccessToken(app))
                .build()
        }
*/
        return null
    }

    private fun responseCount(response: Response): Int {
        var result = 1
        var failed: Response? = response

        while (failed?.priorResponse() != null) {
            result++
            failed = failed.priorResponse()
        }

        return result
    }
}