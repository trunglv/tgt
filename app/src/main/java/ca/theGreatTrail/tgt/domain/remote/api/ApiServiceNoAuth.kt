package ca.theGreatTrail.tgt.domain.remote.api

import io.reactivex.Observable
import retrofit2.http.*

interface ApiServiceNoAuth {

    /*configuration*/
    @GET("configuration/v1/get_config")
    fun getConfig(
        @Query("version_code") versionCode: Int,
        @Query("device_type") deviceType: String = "ANDROID"
    ): Observable<Any>

}