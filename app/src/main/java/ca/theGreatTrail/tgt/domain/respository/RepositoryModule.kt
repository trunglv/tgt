package ca.theGreatTrail.tgt.domain.respository

import android.app.Application
import androidx.room.Room
import ca.theGreatTrail.tgt.data.AppDb
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class RepositoryModule {

    @Provides
    @Singleton
    fun provideDatabase(application: Application): AppDb {
        return Room.databaseBuilder(application, AppDb::class.java, "space_share_cms.db").build()
    }
}