package ca.theGreatTrail.tgt.app

import ca.theGreatTrail.tgt.domain.remote.pojo.response.BaseResponse
import io.reactivex.Observable
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST

interface ApiService {

    /*authentication*/

    @POST("/authentication/v1/partner-reset-password/")
    @FormUrlEncoded
    fun sendEmailResetPassword(
        @Field("email") email: String
    ): Observable<BaseResponse<Any>>
}