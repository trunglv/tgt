package ca.theGreatTrail.tgt.app

import android.app.Application
import ca.theGreatTrail.tgt.domain.remote.api.ApiModule
import ca.theGreatTrail.tgt.domain.remote.api.ApiServiceNoAuth
import ca.theGreatTrail.tgt.domain.respository.RepositoryModule
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [(AppModule::class), (ApiModule::class), (RepositoryModule::class)])
interface AppComponent {

    // <editor-fold defaultstate="collapsed" desc="module-provider">
    fun application(): Application

    fun apiService(): ApiService

    fun apiServiceNoAuth(): ApiServiceNoAuth


    interface Injectable {
        fun inject(appComponent: AppComponent)
    }

    // <editor-fold defaultstate="collapsed" desc="viewModel-injections">
    fun inject(app: App)

//    fun inject(viewModel: SplashViewModel)

}